require 'net/http'
require 'uri'
require 'json'
require 'date'
class NominationsController < ApplicationController
       if($userRole_frm == "Supplier")
            $user_id = $user_id_supplier
        elsif($userRole_frm == "Transporter")
            $user_id = $user_id_transporter 
        end
    
    def index
        if($userRole_frm == "Supplier")
            $user_id = $user_id_supplier
        elsif($userRole_frm == "Transporter")
            $user_id = $user_id_transporter 
        end
       uri = URI('http://gas-poc4.cfapps.io/GAS/services/viewnomination/')
        http = Net::HTTP.new(uri.host, uri.port)
        req = Net::HTTP::Post.new(uri.path, {'Content-Type' => 'application/json'})
        puts $user_id
        req.body = {"userid" => $user_id}.to_json
        res = http.request(req)
        hash_response  = JSON.parse(res.body)
        @nominations = hash_response         
        respond_to do |format|
          format.html  
         format.json  { render :json => @nominations }
        end
    end
    
   def new
        @nomination = ""
        @status = params[:status]
        @capacityRef = params[:capacity_ref]
        @fname = params[:fname]
        @startdate_and_time = params[:startdate_and_time]
        @enddate_and_time = params[:enddate_and_time]
        @injection_point = params[:injection_point]
        @injection_quantity = params[:injection_quantity]
        @offtake_point = params[:offtake_point]
        @offtake_quantity = params[:offtake_quantity]
        @nomination_type = params[:nomination_type]
        @statusNomination = params[:statusNomination]
        @nominationid = params[:nominationid]
        

   end
    
                                 
    def show
        @window = params[:id]
        if(@window == "notification")
            begin
                uri = URI('http://gas-poc4.cfapps.io/GAS/services/viewnotification/')
                http = Net::HTTP.new(uri.host, uri.port)
                req = Net::HTTP::Post.new(uri.path, {'Content-Type' => 'application/json'})
                req.body = {"userid" => $user_id.to_i}.to_json 
                res = http.request(req)
                hash_response  = JSON.parse(res.body)
                @notifications = hash_response 
                respond_to do |format|
                    format.html  {render "nominations/notification"}
                    format.json  { render :json => @notifications }
                end
            end
        elsif(@window == "schedule")
             begin
                uri = URI('http://gas-poc4.cfapps.io/GAS/services/viewscheduler/')
                http = Net::HTTP.new(uri.host, uri.port)
                req = Net::HTTP::Post.new(uri.path, {'Content-Type' => 'application/json'})
                req.body = { "userid" => "All"}.to_json 
                res = http.request(req)
                
    
                hash_response  = JSON.parse(res.body)
                @newHash={}
                 @splitarr=[]
                
                hash_response.each do |key,value|
                    value.each do |k,v|
                        i=0
                        v.each do |vsche|
                               @splitarr =[]
                           @splitarr<< vsche["date_and_time"].split(" ")
                            @splitarr << vsche["injection_point"] +" - " +vsche["injection_quantity"].to_s 
                            @splitarr << vsche["offtake_point"] +" - " +vsche["offtake_quantity"].to_s
                            @newHash.merge!(i => @splitarr)
                           i=i+1
                        end
                        end
                end
                 
                 puts "newHash"
                 puts @newHash
                 @renderHash = {}
                @renderHashOne ={}
                 @renderHashTwo = {}
                 @renderHashThree ={}
                 @renderHashFour = {}
                
                 j=0
                 @newHash.each do |k,v|
                     time = v[0][1].split(':')
                      @detailsFirst = []
                    @detailsSecond = []
                    @detailsThird = []
                    @detailsFour = []
                        
                           if time[0].to_i >=1 and time[0].to_i <=6
                               @detailsFirst << Date.today.to_s
                               @detailsFirst << "1.00 to 6.00"
                                @detailsFirst << v[1]
                                @detailsFirst << v[2]
                               
                               @renderHashOne.merge!(j => @detailsFirst)
                            elsif time[0].to_i >=7 and time[0].to_i <=12
                               @detailsSecond << Date.today.to_s
                               @detailsSecond << "7.00 to 12.00"
                                @detailsSecond << v[1]
                                @detailsSecond << v[2]
                               @renderHashTwo.merge!(j => @detailsSecond)
                            elsif time[0].to_i >=13 and time[0].to_i <=18
                               @detailsThird << Date.today.to_s
                               @detailsThird << "13.00 to 18.00"
                                @detailsThird << v[1]
                                @detailsThird << v[2]
                               @renderHashThree.merge!(j => @detailsThird)
                            elsif time[0].to_i >=19 and time[0].to_i <=23
                               @detailsFour << Date.today.to_s
                               @detailsFour << "19.00 to 24.00"
                                @detailsFour << v[1]
                                @detailsFour << v[2]
                                @renderHashFour.merge!(j => @detailsFour)
                            end
                        
                j=j+1
                end
                    
                @renderHash.merge!(:one => @renderHashOne)
                @renderHash.merge!(:two => @renderHashTwo)
                @renderHash.merge!(:three => @renderHashThree)
                @renderHash.merge!(:four =>@renderHashFour)
                @schedules = @renderHash   
                respond_to do |format|
                    format.html  {render "nominations/schedule"}
                    format.json  { render :json => @schedules }
                end
            end
        end
        if params[:method] == "execute"
            begin
                uri = URI('http://gas-poc4.cfapps.io/GAS/services/triggerscheduler/')
                http = Net::HTTP.new(uri.host, uri.port)
                req = Net::HTTP::Get.new(uri.path)
                res = http.request(req)
            end
        end
     end
    
    def create
        @startDate = params["nominationstartdate"].to_s+"T"+params["nominationstarttime"]+":00"
        @endDate = params["nominationenddate"].to_s+"T"+params["nominationendtime"]+":00"
        if params[:hiddenStatus] == "Create"
            begin
                
                uri = URI('http://gas-poc4.cfapps.io/GAS/services/addnomination/')
                http = Net::HTTP.new(uri.host, uri.port)
                req = Net::HTTP::Post.new(uri.path, {'Content-Type' => 'application/json'})
                req.body = { "startdate_and_time" => @startDate.to_s,
                            "enddate_and_time" =>  @endDate.to_s,
                            "injection_point" => params[:injectionpoint],
                            "injection_quantity" => params[:injectionquantity].to_i,
                            "offtake_point" => params[:offtakepoint],
                            "offtake_quantity" => params[:offtakequantity].to_i,
                            "capacity_ref" => params[:capacity],
                            "status" => "Under Review",
                            "uid" => ($user_id_supplier.to_i)}.to_json 
                res = http.request(req)
                data_hash = JSON.parse(res.body)
                redirect_to :controller => "nominations", :action => "index"  
            end
        elsif params[:hiddenStatus] == "Modify"
        begin                
                uri = URI('http://gas-poc4.cfapps.io/GAS/services/modifynomination/')
                http = Net::HTTP.new(uri.host, uri.port)
                req = Net::HTTP::Post.new(uri.path, {'Content-Type' => 'application/json'})
                req.body = {"nominationid" =>params[:hiddenNominationId].to_i,
                            "startdate_and_time" => @startDate.to_s,
                            "enddate_and_time" => @endDate.to_s,
                            "injection_point" => params[:injectionpoint],
                            "injection_quantity" => params[:injectionquantity].to_i,
                            "offtake_point" => params[:offtakepoint],
                            "offtake_quantity" => params[:offtakequantity].to_i,
                            "capacity_ref" => params[:capacity]}.to_json 
                res = http.request(req)
                data_hash = JSON.parse(res.body)
                redirect_to :controller => "nominations", :action => "index"  
            end
           
        end
        
    end       
end
